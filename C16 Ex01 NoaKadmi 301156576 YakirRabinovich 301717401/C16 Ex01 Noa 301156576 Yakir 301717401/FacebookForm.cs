﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;
using C16_Ex01_Noa_301156576_Yakir_301717401.Features;
using C16_Ex01_Noa_301156576_Yakir_301717401.Infrastructure;
using C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents;

namespace C16_Ex01_Noa_301156576_Yakir_301717401
{
    public partial class FacebookForm : Form
    {
        private static readonly string k_AppId = "1265788020100228";
        private static readonly string k_AppSettingsFilePath = "AppSettings.xml";
        private static readonly int k_FacebookCollectionLimit = 25; // Max is 61

        private AppSettings m_AppSettings;
        private AutoFitTextBox m_StatusTextBox;
        private User m_LoggedInUser;
        private PostsLayout m_NewsFeed;
        private MemoriesForm m_MemoriesForm;
        private GuessWhoForm m_GuessWhoForm;
        private bool m_Initialized;

        public FacebookForm()
        {
            InitializeComponent();
            initializeComponent();
        }

        private void initializeComponent()
        {
            m_StatusTextBox = new AutoFitTextBox();
            this.m_StatusTextBox.Location = new System.Drawing.Point(168, 12);
            this.m_StatusTextBox.Multiline = true;
            this.m_StatusTextBox.TabIndex = 0;
            this.m_StatusTextBox.Text = "What\'s on your mind?";
            this.m_StatusTextBox.Size = new Size(this.Width - m_StatusTextBox.Left - 30, 27);
            m_StatusTextBox.SizeChanged += statusTextBox_SizeChanged;
            this.Controls.Add(m_StatusTextBox);

            m_NewsFeed = new PostsLayout();
            this.m_NewsFeed.Location = new Point(m_StatusTextBox.Left, m_ButtonPost.Bottom + 30);
            this.m_NewsFeed.Size = new Size(487, 328);
            this.m_NewsFeed.TabIndex = 5;
            this.m_NewsFeed.AutoScroll = true;
            this.Controls.Add(m_NewsFeed);

            m_Initialized = true;
        }

        private void statusTextBox_SizeChanged(object sender, EventArgs e)
        {
            if (m_Initialized)
            {
                m_ButtonPost.Top = m_StatusTextBox.Bottom + 5;
                m_NewsFeed.Top = m_ButtonPost.Bottom + 20;
                m_NewsFeed.Height = this.Height - m_NewsFeed.Top - 60;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            FacebookService.s_CollectionLimit = k_FacebookCollectionLimit;
            loadOrCreateAppSettings();
        }

        private void loadOrCreateAppSettings()
        {
            try
            {
                m_AppSettings = AppSettings.LoadFromFile(k_AppSettingsFilePath);
            }
            catch (Exception)
            {
                m_AppSettings = new AppSettings();
            }

            this.WindowState = m_AppSettings.FormWindowState;
            m_CheckBoxRememberUser.Checked = m_AppSettings.ShouldRememberUser;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (m_AppSettings.LastAccessToken == null)
            {
                loginToFb();
            }
            else
            {
                m_LoggedInUser = FacebookService.Connect(m_AppSettings.LastAccessToken).LoggedInUser;
            }

            if (m_LoggedInUser != null)
            {
                doAfterLogin();
            }
            else
            {
                MessageBox.Show("Please login to your Facebook account.", "Login");
                disableForm();
            }
        }

        private void disableForm()
        {
            m_ButtonGuessWho.Enabled = false;
            m_ButtonOpenMemoryForm.Enabled = false;
            m_ButtonPost.Enabled = false;
            m_StatusTextBox.Enabled = false;
            m_CheckBoxRememberUser.Enabled = false;
        }

        private void doAfterLogin()
        {
            fetchUserInfo();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            try
            {
                saveAppSettings();
            }
            catch (Exception)
            {
                MessageBox.Show("App settings was not successfully saved.", "App Settings Error");
            }
        }

        private void saveAppSettings()
        {
            m_AppSettings.ShouldRememberUser = m_CheckBoxRememberUser.Checked;
            m_AppSettings.FormWindowState = this.WindowState;

            if (!m_CheckBoxRememberUser.Checked)
            {
                m_AppSettings.LastAccessToken = null;
            }

            m_AppSettings.SaveToFile(k_AppSettingsFilePath);
        }

        private void fetchUserInfo()
        {
            this.Text = m_LoggedInUser.Name;
            m_PictureBoxProfile.LoadAsync(m_LoggedInUser.PictureNormalURL);
            fetchNewsFeed();
        }

        private void fetchNewsFeed()
        {
            FacebookObjectCollection<Post> newsFeed = m_LoggedInUser.NewsFeed;

            if (newsFeed.Count > 0)
            {
                foreach (Post post in newsFeed)
                {
                    m_NewsFeed.Add(post, m_LoggedInUser);
                }
            }
            else
            {
                MessageBox.Show("No news feed to show.", "News Feed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private LoginResult loginToFb()
        {
            LoginResult loginResult = FacebookService.Login(k_AppId,
                "public_profile", "user_about_me", "user_friends",
                "publish_actions", "user_events", "user_hometown",
                "user_likes", "user_photos", "user_posts");

            m_LoggedInUser = loginResult.LoggedInUser;
            m_AppSettings.LastAccessToken = loginResult.AccessToken;

            return loginResult;
        }

        private void m_ButtonPost_Click(object sender, EventArgs e)
        {
            postStatus();
        }

        private void postStatus()
        {
            Status status = null;

            try
            {
                status = m_LoggedInUser.PostStatus(m_StatusTextBox.Text);
            }
            catch (Exception)
            {
                MessageBox.Show(
@"Something went wrong. Status was not posted.
Please make sure the app has permission to publish.", 
                    "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (status != null)
            {
                refetchNewsFeed();
            }
        }

        private void refetchNewsFeed()
        {
            m_LoggedInUser.ReFetch();
            m_NewsFeed.Clear();
            fetchNewsFeed();
        }

        private void buttonOpenMemoryForm_Click(object sender, EventArgs e)
        {
            if (m_MemoriesForm == null) 
            {
                m_MemoriesForm = new MemoriesForm(m_LoggedInUser);
            }

            m_MemoriesForm.ShowDialog();
        }

        private void buttonGuessWho_Click(object sender, EventArgs e)
        {
            if (m_GuessWhoForm == null)
            {
                m_GuessWhoForm = new GuessWhoForm(m_LoggedInUser);
            }

            m_GuessWhoForm.ShowDialog();
        }
    }
}
