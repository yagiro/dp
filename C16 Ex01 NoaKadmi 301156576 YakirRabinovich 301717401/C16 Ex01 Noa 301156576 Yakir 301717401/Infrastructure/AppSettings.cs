﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.Infrastructure
{
    public class AppSettings
    {
        public string LastAccessToken { get; set; }

        public bool ShouldRememberUser { get; set; }

        public FormWindowState FormWindowState { get; set; }

        public static AppSettings LoadFromFile(string i_FilePath)
        {
            AppSettings loadedAppSettings = null;

            using (FileStream settingsFileStream = new FileStream(i_FilePath, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(AppSettings));
                loadedAppSettings = serializer.Deserialize(settingsFileStream) as AppSettings;
            }

            return loadedAppSettings;
        }

        public void SaveToFile(string i_FilePath)
        {
            using (FileStream settingsFileStream = new FileStream(i_FilePath, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(AppSettings));
                serializer.Serialize(settingsFileStream, this);
            }
        }
    }
}
