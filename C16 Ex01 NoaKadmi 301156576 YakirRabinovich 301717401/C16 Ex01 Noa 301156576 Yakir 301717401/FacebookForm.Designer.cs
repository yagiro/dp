﻿using C16_Ex01_Noa_301156576_Yakir_301717401.Features;

namespace C16_Ex01_Noa_301156576_Yakir_301717401
{
    public partial class FacebookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ButtonPost = new System.Windows.Forms.Button();
            this.m_ButtonOpenMemoryForm = new System.Windows.Forms.Button();
            this.m_PictureBoxProfile = new System.Windows.Forms.PictureBox();
            this.m_CheckBoxRememberUser = new System.Windows.Forms.CheckBox();
            this.m_ButtonGuessWho = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_PictureBoxProfile)).BeginInit();
            this.SuspendLayout();
            // 
            // m_ButtonPost
            // 
            this.m_ButtonPost.BackColor = System.Drawing.SystemColors.Control;
            this.m_ButtonPost.Location = new System.Drawing.Point(580, 65);
            this.m_ButtonPost.Name = "m_ButtonPost";
            this.m_ButtonPost.Size = new System.Drawing.Size(75, 29);
            this.m_ButtonPost.TabIndex = 1;
            this.m_ButtonPost.Text = "Post";
            this.m_ButtonPost.UseVisualStyleBackColor = false;
            this.m_ButtonPost.Click += new System.EventHandler(this.m_ButtonPost_Click);
            // 
            // m_ButtonOpenMemoryForm
            // 
            this.m_ButtonOpenMemoryForm.Location = new System.Drawing.Point(25, 154);
            this.m_ButtonOpenMemoryForm.Name = "m_ButtonOpenMemoryForm";
            this.m_ButtonOpenMemoryForm.Size = new System.Drawing.Size(109, 28);
            this.m_ButtonOpenMemoryForm.TabIndex = 2;
            this.m_ButtonOpenMemoryForm.Text = "Memories";
            this.m_ButtonOpenMemoryForm.UseVisualStyleBackColor = true;
            this.m_ButtonOpenMemoryForm.Click += new System.EventHandler(this.buttonOpenMemoryForm_Click);
            // 
            // m_PictureBoxProfile
            // 
            this.m_PictureBoxProfile.Location = new System.Drawing.Point(23, 12);
            this.m_PictureBoxProfile.Name = "m_PictureBoxProfile";
            this.m_PictureBoxProfile.Size = new System.Drawing.Size(122, 114);
            this.m_PictureBoxProfile.TabIndex = 3;
            this.m_PictureBoxProfile.TabStop = false;
            // 
            // m_CheckBoxRememberUser
            // 
            this.m_CheckBoxRememberUser.AutoSize = true;
            this.m_CheckBoxRememberUser.Location = new System.Drawing.Point(12, 125);
            this.m_CheckBoxRememberUser.Name = "m_CheckBoxRememberUser";
            this.m_CheckBoxRememberUser.Size = new System.Drawing.Size(122, 23);
            this.m_CheckBoxRememberUser.TabIndex = 4;
            this.m_CheckBoxRememberUser.Text = "Remember me";
            this.m_CheckBoxRememberUser.UseVisualStyleBackColor = true;
            // 
            // m_ButtonGuessWho
            // 
            this.m_ButtonGuessWho.Location = new System.Drawing.Point(25, 188);
            this.m_ButtonGuessWho.Name = "m_ButtonGuessWho";
            this.m_ButtonGuessWho.Size = new System.Drawing.Size(109, 28);
            this.m_ButtonGuessWho.TabIndex = 5;
            this.m_ButtonGuessWho.Text = "Guess Who?";
            this.m_ButtonGuessWho.UseVisualStyleBackColor = true;
            this.m_ButtonGuessWho.Click += new System.EventHandler(this.buttonGuessWho_Click);
            // 
            // FacebookForm
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(670, 494);
            this.Controls.Add(this.m_ButtonGuessWho);
            this.Controls.Add(this.m_CheckBoxRememberUser);
            this.Controls.Add(this.m_PictureBoxProfile);
            this.Controls.Add(this.m_ButtonOpenMemoryForm);
            this.Controls.Add(this.m_ButtonPost);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FacebookForm";
            this.Text = "Facebook";
            ((System.ComponentModel.ISupportInitialize)(this.m_PictureBoxProfile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_ButtonPost;
        private System.Windows.Forms.Button m_ButtonOpenMemoryForm;
        private System.Windows.Forms.PictureBox m_PictureBoxProfile;
        private System.Windows.Forms.CheckBox m_CheckBoxRememberUser;
        private System.Windows.Forms.Button m_ButtonGuessWho;
    }
}