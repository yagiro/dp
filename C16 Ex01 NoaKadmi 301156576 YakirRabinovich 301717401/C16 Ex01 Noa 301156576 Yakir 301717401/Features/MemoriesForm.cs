﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;
using C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.Features
{
    public partial class MemoriesForm : Form
    {
        private User m_LoggedInUser;
        private PostsLayout m_MutualPosts;

        public MemoriesForm(User i_LoggedInUser)
        {
            InitializeComponent();
            initializeComponent();

            m_LoggedInUser = i_LoggedInUser;
        }

        private void initializeComponent()
        {
            m_MutualPosts = new PostsLayout();
            m_MutualPosts.Location = new Point(306, 31);
            m_MutualPosts.Size = new Size(495, 194);
            this.Controls.Add(m_MutualPosts);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            clearMutualPhotosGallery();
            clearMutualPosts();
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            fetchFriends();
        }

        private void clearMutualPhotosGallery()
        {
            m_LayoutPannelMutualPhotos.Controls.Clear();
        }

        private void fetchFriends()
        {
            m_ListBoxFriends.Items.Clear();
            m_ListBoxFriends.DisplayMember = "Name";
            FacebookObjectCollection<User> friends = m_LoggedInUser.Friends;
            foreach (User friend in m_LoggedInUser.Friends)
            {
                m_ListBoxFriends.Items.Add(friend);
                friend.ReFetch(DynamicWrapper.eLoadOptions.Full);
            }

            if (m_LoggedInUser.Friends.Count == 0)
            {
                MessageBox.Show("No Friends to retrieve.", "Friends", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonGetMemories_Click(object sender, EventArgs e)
        {
            fetchMemories();
        }

        private void fetchMemories()
        {
            if (hasChosenFriends())
            {
                fetchAndShowMutualPosts();
                fetchAndShowMutualPhotos();
            }
        }

        private void fetchAndShowMutualPosts()
        {
            FacebookObjectCollection<Post> mutualPosts = getMutualPosts();

            clearMutualPosts();

            if (mutualPosts.Count > 0)
            {
                showMutualPosts(mutualPosts);
            }
            else
            {
                MessageBox.Show("No mutual posts to show.", "Mutual Posts", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private FacebookObjectCollection<Post> getMutualPosts()
        {
            FacebookObjectCollection<Post> mutualPosts = new FacebookObjectCollection<Post>();

            User chosenFriend = (User)m_ListBoxFriends.SelectedItem;
            FacebookObjectCollection<Post> postsUserIsTaggedIn = m_LoggedInUser.Posts;
            FacebookObjectCollection<Post> postsFriendIsTaggedIn = chosenFriend.PostsTaggedIn;

            // user
            foreach (Post userPost in postsUserIsTaggedIn)
            {
                bool allFriendsTagged = true;
                foreach (User friend in m_ListBoxFriends.SelectedItems)
                {
                    if (!userIsTaggedIn(userPost, friend))
                    {
                        allFriendsTagged = false;
                    }
                }

                if (allFriendsTagged)
                {
                    mutualPosts.Add(userPost);
                }
            }

            return mutualPosts;
        }

        private bool userIsTaggedIn(Post i_Post, User i_User)
        {
            bool isTagged = false;

            FacebookObjectCollection<User> targetUsers = i_Post.TargetUsers;

            if (targetUsers != null)
            {
                foreach (User taggedUser in i_Post.TargetUsers)
                {
                    if (taggedUser.Id == i_User.Id)
                    {
                        isTagged = true;
                        break;
                    }
                }
            }

            return isTagged;
        }

        private void showMutualPosts(FacebookObjectCollection<Post> i_MutualPosts)
        {
            foreach (Post mutualPost in i_MutualPosts)
            {
                m_MutualPosts.Add(mutualPost, m_LoggedInUser);
            }
        }

        private void clearMutualPosts()
        {
            m_MutualPosts.Clear();
        }

        private void fetchAndShowMutualPhotos()
        {
            FacebookObjectCollection<Photo> mutualPhotos = getMutualPhotos();

            clearMutualPhotosGallery();

            if (mutualPhotos.Count > 0)
            {
                showMutualPhotos(mutualPhotos);
            }
            else
            {
                MessageBox.Show("No mutual photos to show.", "Mutual Photos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private FacebookObjectCollection<Photo> getMutualPhotos()
        {
            FacebookObjectCollection<Photo> mutualPhotos = new FacebookObjectCollection<Photo>();
            FacebookObjectCollection<Photo> photosUserIsTaggedIn = m_LoggedInUser.PhotosTaggedIn;

            foreach (Photo userPhoto in photosUserIsTaggedIn)
            {
                bool allFriendsTagged = true;
                foreach (User friend in m_ListBoxFriends.SelectedItems)
                {
                    if (!userIsTaggedIn(userPhoto, friend))
                    {
                        allFriendsTagged = false;
                    }
                }

                if (allFriendsTagged)
                {
                    mutualPhotos.Add(userPhoto);
                }
            }

            return mutualPhotos;
        }

        private bool userIsTaggedIn(Photo i_Photo, User i_Friend)
        {
            bool isTagged = false;

            foreach(PhotoTag photoTag in i_Photo.Tags)
            {
                if (photoTag.User.Id == i_Friend.Id)
                {
                    isTagged = true;
                    break;
                }
            }

            return isTagged;
        }

        private void showMutualPhotos(FacebookObjectCollection<Photo> mutualPhotos)
        {
            PictureBox addedPictureBox;
            int initialLeft = 327;
            int left = initialLeft;
            int top = 251;
            int i = 1;
            int numOfThumbsInRow = 3;
            foreach(Photo photo in mutualPhotos)
            {
                addedPictureBox = addToMutualPhotosGallery(photo, left, top);
                left += addedPictureBox.Width + 35;
                if (i++ == numOfThumbsInRow)
                {
                    i = 1;
                    left = initialLeft;
                    top += addedPictureBox.Height + 40;
                }
            }
        }

        private PictureBox addToMutualPhotosGallery(Photo i_FbPhoto, int i_Left, int i_Top)
        {
            PictureBox pictureBox = new PictureBox();
            pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox.LoadAsync(i_FbPhoto.ThumbURL);
            pictureBox.Location = new System.Drawing.Point(i_Left, i_Top);
            m_LayoutPannelMutualPhotos.Controls.Add(pictureBox);

            return pictureBox;
        }

        private bool hasChosenFriends()
        {
            bool hasYearAndFriend = true;
            User chosenFriend = (User)m_ListBoxFriends.SelectedItem;

            if (chosenFriend == null)
            {
                MessageBox.Show("Please select one of your friends first.");
                hasYearAndFriend = false;
            }

            return hasYearAndFriend;
        }
    }
}
