﻿namespace C16_Ex01_Noa_301156576_Yakir_301717401.Features
{
    public partial class GuessWhoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ButtonNextQuestion = new System.Windows.Forms.Button();
            this.m_LabelCurrPost = new System.Windows.Forms.Label();
            this.m_AnswersLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.m_LabelWhoPosted = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_ButtonNextQuestion
            // 
            this.m_ButtonNextQuestion.Location = new System.Drawing.Point(425, 418);
            this.m_ButtonNextQuestion.Margin = new System.Windows.Forms.Padding(4);
            this.m_ButtonNextQuestion.Name = "m_ButtonNextQuestion";
            this.m_ButtonNextQuestion.Size = new System.Drawing.Size(217, 34);
            this.m_ButtonNextQuestion.TabIndex = 0;
            this.m_ButtonNextQuestion.Text = "Next Post";
            this.m_ButtonNextQuestion.UseVisualStyleBackColor = true;
            this.m_ButtonNextQuestion.Click += new System.EventHandler(this.buttonNextQuestion_Click);
            // 
            // m_LabelCurrPost
            // 
            this.m_LabelCurrPost.AutoSize = true;
            this.m_LabelCurrPost.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelCurrPost.Location = new System.Drawing.Point(382, 86);
            this.m_LabelCurrPost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.m_LabelCurrPost.Name = "m_LabelCurrPost";
            this.m_LabelCurrPost.Size = new System.Drawing.Size(0, 33);
            this.m_LabelCurrPost.TabIndex = 1;
            this.m_LabelCurrPost.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // m_AnswersLayout
            // 
            this.m_AnswersLayout.Location = new System.Drawing.Point(425, 66);
            this.m_AnswersLayout.Name = "m_AnswersLayout";
            this.m_AnswersLayout.Size = new System.Drawing.Size(217, 325);
            this.m_AnswersLayout.TabIndex = 2;
            // 
            // m_LabelWhoPosted
            // 
            this.m_LabelWhoPosted.AutoSize = true;
            this.m_LabelWhoPosted.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelWhoPosted.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.m_LabelWhoPosted.Location = new System.Drawing.Point(420, 23);
            this.m_LabelWhoPosted.Name = "m_LabelWhoPosted";
            this.m_LabelWhoPosted.Size = new System.Drawing.Size(303, 29);
            this.m_LabelWhoPosted.TabIndex = 3;
            this.m_LabelWhoPosted.Text = "Can you guess who posted it?";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(21, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Here\'s a random post";
            // 
            // GuessWhoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 485);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_LabelWhoPosted);
            this.Controls.Add(this.m_AnswersLayout);
            this.Controls.Add(this.m_LabelCurrPost);
            this.Controls.Add(this.m_ButtonNextQuestion);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GuessWhoForm";
            this.Text = "Guess Who?";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_ButtonNextQuestion;
        private System.Windows.Forms.Label m_LabelCurrPost;
        private System.Windows.Forms.FlowLayoutPanel m_AnswersLayout;
        private System.Windows.Forms.Label m_LabelWhoPosted;
        private System.Windows.Forms.Label label1;
    }
}