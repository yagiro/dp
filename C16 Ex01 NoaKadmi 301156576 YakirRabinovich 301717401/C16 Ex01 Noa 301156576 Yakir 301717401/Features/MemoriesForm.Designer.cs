﻿namespace C16_Ex01_Noa_301156576_Yakir_301717401.Features
{
    public partial class MemoriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ListBoxFriends = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_ButtonGetMemories = new System.Windows.Forms.Button();
            this.m_LayoutPannelMutualPhotos = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // m_ListBoxFriends
            // 
            this.m_ListBoxFriends.FormattingEnabled = true;
            this.m_ListBoxFriends.ItemHeight = 19;
            this.m_ListBoxFriends.Location = new System.Drawing.Point(12, 45);
            this.m_ListBoxFriends.Name = "m_ListBoxFriends";
            this.m_ListBoxFriends.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.m_ListBoxFriends.Size = new System.Drawing.Size(256, 365);
            this.m_ListBoxFriends.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pick friends";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(304, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mutual Posts";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "Mutual Photos";
            // 
            // m_ButtonGetMemories
            // 
            this.m_ButtonGetMemories.Location = new System.Drawing.Point(12, 432);
            this.m_ButtonGetMemories.Name = "m_ButtonGetMemories";
            this.m_ButtonGetMemories.Size = new System.Drawing.Size(256, 50);
            this.m_ButtonGetMemories.TabIndex = 7;
            this.m_ButtonGetMemories.Text = "Find Mutual Memories";
            this.m_ButtonGetMemories.UseVisualStyleBackColor = true;
            this.m_ButtonGetMemories.Click += new System.EventHandler(this.buttonGetMemories_Click);
            // 
            // m_LayoutPannelMutualPhotos
            // 
            this.m_LayoutPannelMutualPhotos.AutoScroll = true;
            this.m_LayoutPannelMutualPhotos.Location = new System.Drawing.Point(308, 257);
            this.m_LayoutPannelMutualPhotos.Name = "m_LayoutPannelMutualPhotos";
            this.m_LayoutPannelMutualPhotos.Size = new System.Drawing.Size(503, 225);
            this.m_LayoutPannelMutualPhotos.TabIndex = 8;
            // 
            // MemoriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(842, 498);
            this.Controls.Add(this.m_LayoutPannelMutualPhotos);
            this.Controls.Add(this.m_ButtonGetMemories);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_ListBoxFriends);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MemoriesForm";
            this.Text = "Mutual Group Memories";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox m_ListBoxFriends;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button m_ButtonGetMemories;
        private System.Windows.Forms.FlowLayoutPanel m_LayoutPannelMutualPhotos;
    }
}