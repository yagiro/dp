﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;
using C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents;
using C16_Ex01_Noa_301156576_Yakir_301717401.Infrastructure;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.Features
{
    public partial class GuessWhoForm : Form
    {
        private AnonymousPostBox m_CurrPostBox;
        private Post m_CurrPost;
        private Post m_NextPost;
        private User m_LoggedInUser;
        private FacebookObjectCollection<User> m_Answers;
        private FacebookObjectCollection<User> m_Friends;
        private FacebookObjectCollection<Post> m_UnusedRandomPosts;
        private FacebookObjectCollection<Post> m_UsedRandomPosts;
        private Random m_Random;
        private int m_NumOfAnswersToShow = 3;

        public GuessWhoForm(User i_LoggedInUser)
        {
            m_LoggedInUser = i_LoggedInUser;
            m_Friends = new FacebookObjectCollection<User>();
            m_Answers = new FacebookObjectCollection<User>();
            m_UnusedRandomPosts = new FacebookObjectCollection<Post>();
            m_UsedRandomPosts = new FacebookObjectCollection<Post>();
            m_Random = new Random();

            InitializeComponent();
            initializeComponent();
        }

        private void initializeComponent()
        {
            m_CurrPostBox = new AnonymousPostBox(m_LoggedInUser);
            m_CurrPostBox.ShowSender = false;
            m_CurrPostBox.Size = new Size(350, 300);
            m_CurrPostBox.Location = new Point(30, 30);
            this.Controls.Add(m_CurrPostBox);
        }

        protected override void OnShown(EventArgs e)
        {
            fetchRandomPosts();
            startGame();
        }

        private void startGame()
        {
            m_NextPost = getRandomUnusedPost();
            presentNextQuestion();
        }

        private void presentNextQuestion()
        {
            m_CurrPost = m_NextPost;

            if (m_CurrPost != null)
            {
                presentRandomPost();
                presentAnswers();
            }

            m_NextPost = getRandomUnusedPost();

            if (m_NextPost == null)
            {
                onAllPostsUsed();
            }
        }

        private void presentRandomPost()
        {
            m_CurrPostBox.Post = m_CurrPost;
        }

        private void presentAnswers()
        {
            getAnswers();

            foreach (Control answerUserPhoto in m_AnswersLayout.Controls)
            {
                answerUserPhoto.Cursor = Cursors.Hand;
                answerUserPhoto.Click += answerUserPhoto_Click;
            }
        }

        private void clearAnswers()
        {
            foreach (Control answerUserPhoto in m_AnswersLayout.Controls)
            {
                answerUserPhoto.Click -= answerUserPhoto_Click;
            }

            m_AnswersLayout.Controls.Clear();
        }

        private void answerUserPhoto_Click(object sender, EventArgs e)
        {
            UserPhoto chosenAnswer = sender as UserPhoto;

            if (chosenAnswer.User.Id == m_CurrPost.From.Id)
            {
                MessageBox.Show("Correct!", "Good", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else 
            {
                MessageBox.Show("Wrong answer.", "Nope", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void getAnswers()
        {
            clearAnswers();

            // add the correct answer
            m_AnswersLayout.Controls.Add(new UserPhoto(m_CurrPost.From));

            // -1 because we already added the correct answer
            if (m_Friends.Count <= m_NumOfAnswersToShow - 1)
            {
                foreach (User friend in m_Friends)
                {
                    m_AnswersLayout.Controls.Add(new UserPhoto(friend));
                }
            }
            else
            {
                getRandomAnswers(m_NumOfAnswersToShow - 1);
            }
        }

        private void getRandomAnswers(int i_Amount)
        {
            User randomUnusedUser;
            FacebookObjectCollection<User> unusedFriends = new FacebookObjectCollection<User>();

            foreach (User friend in m_Friends)
            {
                if (friend.Id != m_CurrPost.From.Id)
                {
                    unusedFriends.Add(friend);
                }
            }

            for (int i = 0; i < i_Amount; ++i)
            {
                randomUnusedUser = null;

                if (unusedFriends.Count > 0)
                {
                    int randomIndex = m_Random.Next(unusedFriends.Count);
                    randomUnusedUser = unusedFriends[randomIndex];
                }

                if (randomUnusedUser != null)
                {
                    m_AnswersLayout.Controls.Add(new UserPhoto(randomUnusedUser));
                    unusedFriends.Remove(randomUnusedUser);
                }
            }
        }

        private string getPostTextualContent(Post i_Post)
        {
            string textualContent = null;

            switch (i_Post.Type)
            {
                case Post.eType.photo:
                    textualContent = i_Post.Message;
                    break;
                case Post.eType.link:
                    textualContent = i_Post.Link;
                    break;
                case Post.eType.video:
                    textualContent = i_Post.Link;
                    break;
                default:
                    textualContent = i_Post.Message;
                    break;
            }

            return textualContent;
        }

        private void onAllPostsUsed()
        {
            MessageBox.Show("No more posts to show.");
        }

        private void setPostAsUsed(Post i_RandomPost)
        {
            m_UsedRandomPosts.Add(i_RandomPost);
            m_UnusedRandomPosts.Remove(i_RandomPost);
        }

        private Post getRandomUnusedPost()
        {
            Post randomUnusedPost = null;

            if (m_UnusedRandomPosts.Count > 0)
            {
                int randomIndex = m_Random.Next(m_UnusedRandomPosts.Count);
                randomUnusedPost = m_UnusedRandomPosts[randomIndex];
                setPostAsUsed(randomUnusedPost);
            }

            return randomUnusedPost;
        }

        private void fetchFriends()
        {
            m_Friends = m_LoggedInUser.Friends;
        }

        private void fetchRandomPosts()
        {
            fetchFriends();

            foreach (User friend in m_Friends)
            {
                foreach (Post friendPots in friend.Posts)
                {
                    m_UnusedRandomPosts.Add(friendPots);
                }
            }
        }

        private void buttonNextQuestion_Click(object sender, EventArgs e)
        {
            presentNextQuestion();
        }
    }
}
