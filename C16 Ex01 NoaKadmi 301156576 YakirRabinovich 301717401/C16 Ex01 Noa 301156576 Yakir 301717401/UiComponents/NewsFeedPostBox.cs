﻿using System;
using System.Windows.Forms;
using System.Drawing;
using FacebookWrapper.ObjectModel;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public class NewsFeedPostBox : PostBox
    {
        private readonly Size r_DefaultSize = new Size(465, 150);

        public NewsFeedPostBox(User i_LoggedInUser)
            : base(i_LoggedInUser)
        {
        }

        public NewsFeedPostBox(Post i_Post, User i_LoggedInUser)
            : base(i_Post, i_LoggedInUser)
        {
        }
    }
}
