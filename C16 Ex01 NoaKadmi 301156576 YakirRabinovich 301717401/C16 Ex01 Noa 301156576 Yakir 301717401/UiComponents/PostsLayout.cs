﻿using System;
using System.Windows.Forms;
using System.Drawing;

using FacebookWrapper.ObjectModel;
using C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public class PostsLayout : TableLayoutPanel
    {
        public void Add(Post i_Post, User i_LoggedInUser)
        {
            Add(new PostBox(i_Post, i_LoggedInUser));
        }

        public void Add(PostBox i_PostBox)
        {
            i_PostBox.Width = this.Width;
            this.Controls.Add(i_PostBox);
        }

        public void Clear()
        {
            this.Controls.Clear();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            foreach (Control control in this.Controls)
            {
                control.Width = this.Width;
            }
        }
    }
}
