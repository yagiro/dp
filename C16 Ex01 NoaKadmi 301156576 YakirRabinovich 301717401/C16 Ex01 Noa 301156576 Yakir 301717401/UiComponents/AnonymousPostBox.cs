﻿using System;
using System.Windows.Forms;
using System.Drawing;
using FacebookWrapper.ObjectModel;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public class AnonymousPostBox : PostBox
    {
        public AnonymousPostBox(User i_LoggedInUser) : base(i_LoggedInUser)
        {
        }

        public AnonymousPostBox(Post i_Post, User i_LoggedInUser)
            : base(i_Post, i_LoggedInUser)
        {
        }
    }
}
