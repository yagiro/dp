﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public delegate void HeightChangedEventHandler(int i_HeightDelta);

    public class AutoFitTextBox : TextBox
    {
        public event HeightChangedEventHandler HeightChanged;
        
        public int MinHeight { get; set; }
        
        public int MaxHeight { get; set; }

        public bool AutoFitToTextSize { get; set; }

        private const int k_DefaultWidth = 300;
        private int m_LastHeight;

        public AutoFitTextBox()
        {
            MinHeight = 30;
            MaxHeight = 80;
            Size = new Size(k_DefaultWidth, MinHeight);
            AutoFitToTextSize = true;
            Multiline = true;
            m_LastHeight = this.Height;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);

            if (AutoFitToTextSize)
            {
                int oldHeight = this.Height;
                resizeToFitText();
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            int heightChangeDelta = this.Height - m_LastHeight;
            m_LastHeight = this.Height;

            if (heightChangeDelta != 0)
            {
                onHeightChanged(heightChangeDelta);
            }
        }

        private void onHeightChanged(int i_HeightChangeDelta)
        {
            if (HeightChanged != null)
            {
                HeightChanged.Invoke(i_HeightChangeDelta);
            }
        }

        private void resizeToFitText()
        {
            Size textSize = TextRenderer.MeasureText(this.Text, this.Font);

            if (textSize.Height + 20 > MaxHeight)
            {
                this.Height = this.MaxHeight;
            }
            else if (textSize.Height < MinHeight)
            {
                this.Height = MinHeight;
            }
            else
            {
                Height = textSize.Height + 20;
            }

            this.SelectionStart = this.Text.Length;
            this.ScrollToCaret();
        }

        public void ResizeToFitText()
        {
            resizeToFitText();
        }
    }
}