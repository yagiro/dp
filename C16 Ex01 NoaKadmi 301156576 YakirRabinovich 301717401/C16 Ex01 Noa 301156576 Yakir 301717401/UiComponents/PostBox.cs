﻿using System;
using System.Windows.Forms;
using System.Drawing;
using FacebookWrapper.ObjectModel;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public class PostBox : GroupBox
    {
        public bool ShowSender { get; set; }

        public Color LikeColor { get; set; }

        public Post Post
        {
            get { return m_Post; }
            set
            {
                if (m_Post != null)
                {
                    doBeforePostChange();
                }

                m_Post = value;
                onPostChanged();
            }
        }

        protected Post m_Post;
        protected bool m_Initialized;
        protected User m_LoggedInUser;
        protected bool m_IsLiked;
        protected Button m_ButtonLike;
        protected Button m_ButtonComment;
        protected PictureBox m_Photo;
        protected AutoFitTextBox m_CommentTextBox;
        protected Label m_Sender;
        protected TextBox m_MessageTextBox;

        public PostBox(User i_LoggedInUser)
        {
            ShowSender = true;
            m_LoggedInUser = i_LoggedInUser;
            this.Padding = new Padding(10);
            LikeColor = Color.LightGreen;
            initializeComponent();
        }

        public PostBox(Post i_Post, User i_LoggedInUser)
            : this(i_LoggedInUser)
        {
            m_Post = i_Post;
            populatePost();
        }

        private void doBeforePostChange()
        {
            if (m_Post.Type == Post.eType.photo)
            {
                removePhoto();
            }
        }

        private void removePhoto()
        {
            doBeforeRemovingPhoto();
            m_Photo.Visible = false;
            m_Photo.Image.Dispose();
            m_Photo.Image = null;
        }

        private void onPostChanged()
        {
            populatePost();
        }

        protected virtual void populatePost()
        {
            if (m_Initialized)
            {
                m_IsLiked = isLiked();
                m_Sender.Text = ShowSender ? m_Post.From.Name : string.Empty;

                switch (m_Post.Type)
                {
                    case Post.eType.status:
                        m_MessageTextBox.Text = m_Post.Message;
                        break;
                    case Post.eType.link:
                        m_MessageTextBox.Text = m_Post.Link;
                        break;
                    case Post.eType.photo:
                        insertPhoto();
                        break;
                    case Post.eType.video:
                        m_MessageTextBox.Text = m_Post.Link;
                        break;
                    default:
                        m_MessageTextBox.Text = m_Post.Message;
                        break;
                }
            }
        }

        private void insertPhoto()
        {
            m_MessageTextBox.Text = m_Post.Message;
            m_Photo.Load(m_Post.PictureURL);
            makeRoomForPhoto();
        }

        private void makeRoomForPhoto()
        {
            m_Photo.Top = m_Sender.Bottom + m_MessageTextBox.Height + (m_Sender.Height / 2) + 5;
            m_Photo.Visible = true;
            m_ButtonLike.Top = m_Photo.Bottom;
            m_ButtonComment.Top = m_Photo.Bottom;
            m_CommentTextBox.Top = m_ButtonComment.Bottom;
            this.Height += m_Photo.Height;
        }

        private void doBeforeRemovingPhoto()
        {
            m_ButtonLike.Top -= m_Photo.Height;
            m_ButtonComment.Top -= m_Photo.Height;
            m_CommentTextBox.Top -= m_Photo.Height;
            this.Height -= m_Photo.Height;
        }

        protected void designAsPhotoPost()
        {
            m_Photo.Top = m_Sender.Bottom + (m_Sender.Height / 2) + 5;

            int pixelsToPushDown = m_Photo.Height + (2 * m_Sender.Height);

            m_MessageTextBox.Top += pixelsToPushDown;
            m_ButtonComment.Top += pixelsToPushDown;
            m_ButtonLike.Top += pixelsToPushDown;
            m_CommentTextBox.Top += pixelsToPushDown;
            m_Photo.Visible = true;

            this.Height += m_Photo.Height + 50;

            if (m_MessageTextBox.Text.Length == 0)
            {
                int pixelsToPushUp = m_MessageTextBox.Height - m_Sender.Height;

                m_MessageTextBox.Visible = false;
                m_ButtonComment.Top -= pixelsToPushUp;
                m_ButtonLike.Top -= pixelsToPushUp;
                m_CommentTextBox.Top -= pixelsToPushUp;
                this.Height -= pixelsToPushUp;
            }
        }

        protected virtual void initializeComponent()
        {
            m_Sender = new Label();
            m_Sender.AutoSize = true;

            m_Sender.Location = new Point(0, (m_Sender.Height / 2) + 2);
            m_Sender.ForeColor = Color.MidnightBlue;
            this.Controls.Add(m_Sender);

            m_MessageTextBox = new TextBox();
            m_MessageTextBox.Multiline = true;
            m_MessageTextBox.Location = new Point(0, m_Sender.Bottom + 10);
            m_MessageTextBox.Size = new Size(this.Width, 80);
            m_MessageTextBox.ReadOnly = true;
            m_MessageTextBox.BackColor = Color.Lavender;
            this.Controls.Add(m_MessageTextBox);

            m_Photo = new PictureBox();
            m_Photo.SizeMode = PictureBoxSizeMode.AutoSize;
            m_Photo.Visible = false;
            this.Controls.Add(m_Photo);

            m_ButtonLike = new Button();
            m_ButtonLike.Size = new Size(50, 40);
            m_ButtonLike.Location = new Point(0, m_MessageTextBox.Bottom);
            m_ButtonLike.Text = "Like";
            m_ButtonLike.Click += buttonLike_Click;
            m_ButtonLike.BackColor = m_IsLiked ? LikeColor : Color.LightGray;
            this.Controls.Add(m_ButtonLike);

            m_ButtonComment = new Button();
            m_ButtonComment.Enabled = false;
            m_ButtonComment.Size = new Size(80, m_ButtonLike.Height);
            m_ButtonComment.Location = new Point(m_ButtonLike.Right, m_ButtonLike.Top);
            m_ButtonComment.Text = "Comment";
            m_ButtonComment.Click += buttonComment_Click;
            this.Controls.Add(m_ButtonComment);

            m_CommentTextBox = new AutoFitTextBox();
            m_CommentTextBox.Multiline = true;
            m_CommentTextBox.Font = new Font(m_CommentTextBox.Font.Name, 10);
            m_CommentTextBox.Size = new Size(this.Width, 30);
            m_CommentTextBox.Location = new Point(0, m_ButtonComment.Bottom);
            m_CommentTextBox.TextChanged += commentTextBox_TextChanged;
            m_CommentTextBox.HeightChanged += commentTextBox_HeightChanged;
            this.Controls.Add(m_CommentTextBox);

            m_Initialized = true;
            this.Height = 20 + m_Sender.Height + m_MessageTextBox.Height + m_ButtonLike.Height + m_CommentTextBox.Height;
        }

        protected virtual void commentTextBox_HeightChanged(int i_HeightDelta)
        {
            this.Height += i_HeightDelta;
        }

        protected bool isLiked()
        {
            bool isLiked = false;

            try
            {
                FacebookObjectCollection<User> likedBy = m_Post.LikedBy;
                foreach (User likedUser in likedBy)
                {
                    if (likedUser.Id == m_LoggedInUser.Id)
                    {
                        isLiked = true;
                        break;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Facebook wants us to go a little bit slower with the game.", "Facebook Problem", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return isLiked;
        }

        protected virtual void fitComponentsToNewSize()
        {
            if (m_Initialized)
            {
                m_CommentTextBox.Width = this.Width;
                m_ButtonLike.Size = new Size(50, 40);
                m_ButtonComment.Size = new Size(80, m_ButtonLike.Height);
                m_MessageTextBox.Width = this.Width;
            }
        }

        protected void commentTextBox_TextChanged(object sender, EventArgs e)
        {
            m_ButtonComment.Enabled = m_CommentTextBox.Text.Length > 0;
        }

        protected void buttonComment_Click(object sender, EventArgs e)
        {
            Comment postedComment = null;

            try
            {
                postedComment = m_Post.Comment(m_CommentTextBox.Text);

                if (postedComment != null)
                {
                    MessageBox.Show("Your comment was posted!", "Comment", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong. Your comment was not posted.", "Comment", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void buttonLike_Click(object sender, EventArgs e)
        {
            bool actionPosted = false;

            try
            {
                actionPosted = !m_IsLiked ? m_Post.Like() : m_Post.Unlike();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong. Your like was not posted.", "Like", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (actionPosted)
            {
                m_IsLiked = !m_IsLiked;
                m_ButtonLike.BackColor = m_IsLiked ? LikeColor : Color.LightGray;
            }
            else
            {
                MessageBox.Show("Something went wrong. Your like was not posted.", "Like", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            fitComponentsToNewSize();
        }
    }
}
