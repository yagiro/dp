﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace C16_Ex01_Noa_301156576_Yakir_301717401.UiComponents
{
    public class UserPhoto : PictureBox
    {
        public User User { get; private set; }

        public UserPhoto()
        {
            SizeMode = PictureBoxSizeMode.AutoSize;
        }

        public UserPhoto(User i_User) : this()
        {
            User = i_User;
            LoadAsync(User.PictureNormalURL);
        }
    }
}
